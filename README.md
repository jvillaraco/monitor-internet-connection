# monitor internet connection

Monitor internet connection with speedtest and prometheus

## Description
Do we really know our internet connection performance over time?

This project is a part of developing and a part of integrating soltutions to 
answer this question.

## Visuals


## Installation
### Get Docker images
Oficial docker images are referenced in documentation https://prometheus.io/download/. References goes to https://hub.docker.com/r/prom/ user in docker hub.

Info for prometheus:
* Prometheus project in dockerhub: https://hub.docker.com/r/prom/prometheus
* Installation (including Docker): https://prometheus.io/docs/prometheus/latest/installation/
* Grafana support for Prometheus: https://prometheus.io/docs/visualization/grafana/

Info for Grafana:
* Grafana Docker installation: https://grafana.com/docs/grafana/latest/installation/docker/
* Grafana Docker configuration: https://grafana.com/docs/grafana/latest/administration/configure-docker/

Info for node-exporter:
* Dockerhub: https://hub.docker.com/r/prom/node-exporter
* Source for node-exporter: https://github.com/prometheus/node_exporter
* Node-exporter/Prometheus/Grafana: https://grafana.com/docs/grafana-cloud/quickstart/docker-compose-linux/

### Docker config
1. Create docker network (docker/createDockerNetworkPrometheus.sh)
2. Start Prometheus container (docker/startPrometheus.sh)
3. Start Grafana container (docker/startGrafana.sh)

### Node exporter config
Following https://prometheus.io/docs/guides/node-exporter/
1. On debian 11 aka Bullseye install package prometheus-node-exporter with apt install prometheus-node-exporter
2. Test it is working with ```curl http://127.0.0.1:9100/metrics```
3. Modify prometheus.yml with new scrape_config as shown in docker/prometheus.yml. Restart Prometheus container.

### Config and testing Prometheus
Following https://prometheus.io/docs/prometheus/latest/getting_started/ and https://stackoverflow.com/questions/31324981/how-to-access-host-port-from-docker-container
1. Configure prometheus.yml as shown in file
2. Connect to http://127.0.0.1:9090/graph and use the "Graph" tab.
3. Enter the following expression to graph the per-second rate of chunks being created in the self-scraped Prometheus: "rate(prometheus_tsdb_head_chunks_created_total[1m])"


### Config Grafana for Prometheus
1. Change default password
2. Add data source
3. HTTP-URL in data source: http://prometheus:9090
4. Add dashboard
5. Add Panel
6. In metric browser, add "rate(prometheus_http_request_duration_seconds_count{job="prometheus"}[5m])"

## Usage

## Support
This project has no support from owner and maintainer.

## Roadmap


## Contributing
This project is open to contributions, create a new issue is order to conntact
with owner/maintainer.

## License
GNU GENERAL PUBLIC LICENSE Version 2


## Project status
WIP to get prometheus/grafana in a basic working mode.

