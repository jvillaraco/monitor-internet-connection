docker run --detach \
	-p 3000:3000 \
	--network prometheus \
	-v ~/treball/dev/monitor-internet-connection/docker/etc_grafana:/etc/grafana/ \
	-v ~/treball/dev/monitor-internet-connection/docker/var_lib_grafana:/var/lib/grafana/ \
	--name grafana \
grafana/grafana:8.3.3

#	--network prometheus \