docker run --detach \
    --publish 9090:9090 \
    --volume "$(pwd)"/etc_prometheus//prometheus.yml:/etc/prometheus/prometheus.yml \
    --volume "$(pwd)"/prometheus:/prometheus \
    --network prometheus \
    --add-host=host.docker.internal:host-gateway \
    --name prometheus \
prom/prometheus:v2.32.1

  #  --network prometheus \