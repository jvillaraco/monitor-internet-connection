docker run --rm --detach --tty \
    --name node-exporter \
    --network prometheus \
    -p 9100:9100 \
    -v "/proc:/host/proc:ro" \
    -v "/sys:/host/sys:ro" \
    -v "/:/rootfs:ro" \
prom/node-exporter:latest \
    --path.rootfs=/host \


    #--net="host" \
    #--pid="host" \
    #-v "/:/host:ro,rslave" \